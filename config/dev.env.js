var merge = require('webpack-merge')
var prodEnv = require('./prod.env')
var localEnv = require('../.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
}, localEnv)
