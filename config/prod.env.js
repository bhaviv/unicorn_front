var merge = require('webpack-merge')
var prodEnv = require('../.env.production')

module.exports = merge({
  NODE_ENV: '"production"',
}, prodEnv)
