import http from './http.js'

export const ListPlacement = {
  all (siteId) {
    let getListPlacementsUrl = buildListPlacementsUrl(siteId)
    return http.get(getListPlacementsUrl)
  },

  get (siteId, listPlacementId) {
    let getListPlacementUrl = buildListPlacementsUrl(siteId, listPlacementId)
    console.log('getListPlacementUrl', getListPlacementUrl)
    return http.get(getListPlacementUrl)
  },

  save (siteId, listPlacementToSave) {
    if (listPlacementToSave.id) {
      let updateListPlacementUrl = buildListPlacementsUrl(siteId, listPlacementToSave.id)
      return http.put(updateListPlacementUrl, listPlacementToSave)
    } else {
      let saveListPlacementUrl = buildListPlacementsUrl(siteId)
      return http.post(saveListPlacementUrl, listPlacementToSave)
    }
  }
}

function buildListPlacementsUrl (siteId, listPlacementId) {
  let url = 'api/v1/sites/' + siteId + '/list-placements'
  url += (listPlacementId ? '/' + listPlacementId : '') + '.json'

  return url
}
