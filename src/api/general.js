import http from './http.js'

export const ActionTypes = {
  get () {
    return http.get('api/v1/action-types.json')
  }
}

export const RuleTypes = {
  get () {
    return http.get('api/v1/rule-types.json')
  }
}
