import http from './http.js'

export const Experiment = {
  get (lpParams, experimentId) {
    let url = buildExperimentUrl(lpParams, experimentId)
    return http.get(url)
  },

  all (lpParams) {
    let url = buildExperimentUrl(lpParams)
    return http.get(url)
  },

  create (lpParams, experiment) {
    let url = buildExperimentUrl(lpParams, experiment.id)
    return http.post(url, experiment)
  },

  update (lpParams, experiment) {
    let url = buildExperimentUrl(lpParams, experiment.id)
    return http.put(url, experiment)
  },

  delete (lpParams, experiment) {
    let url = buildExperimentUrl(lpParams, experiment.id)
    return http.delete(url)
  },

  clone (lpParams, experiment) {
    let url = buildExperimentUrl(lpParams, experiment.id, 'clone')
    return http.post(url)
  },

  stop (lpParams, experiment) {
    let url = buildExperimentUrl(lpParams, experiment.id, 'stop')
    return http.post(url)
  },

  activate (lpParams, experiment) {
    let url = buildExperimentUrl(lpParams, experiment.id, 'activate')
    return http.post(url)
  }
}

function buildExperimentUrl (lpParams, experimentId, action) {
  let url = 'api/v1/sites/' + lpParams.siteId + '/list-placements/' + lpParams.lpId + '/experiments'
  url += (experimentId ? '/' + experimentId : '') + (action ? '/' + action : '') + '.json'

  return url
}
