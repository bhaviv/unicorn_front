import * as data from './data'
export * from './api/experiment.js'
export * from './api/site.js'

const LATENCY = 1000

export function getActionTypes (cb) {
  setTimeout(() => {
    cb(data.actionTypes)
  }, LATENCY)
}

export function getRuleTypes (cb) {
  setTimeout(() => {
    cb(data.ruleTypes)
  }, LATENCY)
}

export function getBrandsList (listId, cb) {
  setTimeout(() => {
    cb(data.brandsLists[listId])
  }, LATENCY)
}
