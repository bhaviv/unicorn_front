import * as data from '../data'
import {STATUS} from '../../../components/general/constants.js'

export function getExperiment (id, cb) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      let experiment = data.experiments.find(experiment => { return experiment.id === parseInt(id) })
      if (experiment) {
        cb(experiment)
        resolve(experiment)
      } else {
        cb({})
      }
    }, data.LATENCY)
  })
}

export function getExperiments (params, cb) {
  setTimeout(() => {
    let experiments = data.experiments.filter(experiment => {
      return experiment.siteId === parseInt(params.siteId) &&
             experiment.listId === parseInt(params.listId)
    })
    cb(experiments)
  }, data.LATENCY)
}

export function createOrUpdateExperiment (experiment, cb) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      experiment.id = data.counters.autoIncrementExperimentId++
      data.experiments.push(experiment)
      cb(experiment)
      resolve(experiment)
    }, data.LATENCY)
  })
}

export function cloneExperiment (experiment, cb) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      experiment.id = data.counters.autoIncrementExperimentId++
      experiment.status = STATUS.draft
      data.experiments.push(experiment)
      cb(experiment)
      resolve(experiment)
    }, data.LATENCY)
  })
}

export function deleteExperiment (experiment) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      let index = data.experiments.indexOf(experiment)
      if (index !== -1) {
        data.experiments.splice(index, 1)
        resolve()
      } else {
        reject()
      }
    }, data.LATENCY)
  })
}

export function stopExperiment (experiment, cb) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      experiment.status = STATUS.archive
      cb(experiment)
      resolve(experiment)
    }, data.LATENCY)
  })
}

export function activateExperiment (experiment, cb) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      experiment.status = STATUS.active
      cb(experiment)
      resolve(experiment)
    }, data.LATENCY)
  })
}

