import * as data from '../data'

export function getSite (siteId, cb) {
  setTimeout(() => {
    let site = data.sites.find(site => {
      return site.id === parseInt(siteId)
    })
    cb(site)
  }, data.LATENCY)
}

export function getSites (cb) {
  setTimeout(() => {
    cb(data.sites)
  }, data.LATENCY)
}

export function getSiteList (siteId, listId, cb) {
  setTimeout(() => {
    let list = data.sitesLists[siteId].find(list => {
      return list.id === parseInt(listId)
    })
    cb(list)
  }, data.LATENCY)
}

export function getSiteLists (siteId, cb) {
  setTimeout(() => {
    cb(data.sitesLists[siteId])
  }, data.LATENCY)
}
