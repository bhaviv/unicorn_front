export const ruleTypes = [
  {
    slug: 'QueryParam',
    name: 'Query Param'
  },
  {
    slug: 'CookieParam',
    name: 'Cookie param'
  }
]
