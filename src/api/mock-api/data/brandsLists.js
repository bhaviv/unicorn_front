export const brandsList1 = [
  {
    position: 1,
    slug: 'betfair-bingo',
    name: 'Betfair Bingo'
  },
  {
    position: 2,
    slug: 'bingo-diamond',
    name: 'Bingo Diamond'
  },
  {
    position: 3,
    slug: 'butlers-bingo',
    name: 'Butlers Bingo'
  },
  {
    position: 4,
    slug: 'dotty-bingo',
    name: 'Dotty Bingo'
  },
  {
    position: 5,
    slug: 'gala-bingo',
    name: 'Gala Bingo'
  },
  {
    position: 6,
    slug: 'heart-bingo',
    name: 'Heart Bingo'
  },
  {
    position: 7,
    slug: 'ladbrokes-bingo',
    name: 'Ladbrokes Bingo'
  },
  {
    position: 8,
    slug: 'paddy-power-bingo',
    name: 'Paddy Power Bingo'
  },
  {
    position: 9,
    slug: '888-ladies',
    name: '888 Ladies'
  }
]

export const brandsList2 = [
  {
    position: 1,
    slug: 'bingo-diamond',
    name: 'Bingo Diamond'
  },
  {
    position: 2,
    slug: 'butlers-bingo',
    name: 'Butlers Bingo'
  },
  {
    position: 3,
    slug: 'betfair-bingo',
    name: 'Betfair Bingo'
  },
  {
    position: 4,
    slug: 'gala-bingo',
    name: 'Gala Bingo'
  },
  {
    position: 5,
    slug: '888-ladies',
    name: '888 Ladies'
  },
  {
    position: 6,
    slug: 'heart-bingo',
    name: 'Heart Bingo'
  },
  {
    position: 7,
    slug: 'ladbrokes-bingo',
    name: 'Ladbrokes Bingo'
  },
  {
    position: 8,
    slug: 'dotty-bingo',
    name: 'Dotty Bingo'
  },
  {
    position: 9,
    slug: 'paddy-power-bingo',
    name: 'Paddy Power Bingo'
  }
]

export const brandsLists = {
  3: brandsList1,
  8: brandsList2
}
