import * as actions from './actions'

export const experiments = [
  {
    id: 1,
    siteId: 5,
    listId: 8,
    name: 'Experiment 1',
    description: 'Experiment 1 is Running',
    status: 'active',
    rules: {},
    actions: actions.actions1
  },
  {
    id: 2,
    siteId: 5,
    listId: 3,
    name: 'Experiment 2',
    description: 'Experiment 2 is no longer used',
    status: 'archive',
    rules: {
      condition: 'and',
      rules: [
        {
          type: 'QueryParam',
          name: 'aaa',
          operator: 'equal',
          value: 'bbb'
        }
      ]
    },
    actions: actions.actions1
  },
  {
    id: 3,
    siteId: 5,
    listId: 8,
    name: 'Experiment 3',
    description: 'We still working on Experiment 3 ',
    status: 'draft',
    rules: {},
    actions: actions.actions2
  },
  {
    id: 4,
    siteId: 5,
    listId: 8,
    name: 'Experiment 4',
    description: 'Experiment 4 done already',
    status: 'archive',
    rules: {
      condition: 'and',
      rules: [
        {
          type: 'QueryParam',
          name: 'aaa',
          operator: 'equal',
          value: 'bbb'
        }
      ]
    },
    actions: actions.actions2
  }
]
