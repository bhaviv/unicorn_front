export let sites = [
  {
    id: 5,
    name: 'PlayRight'
  },
  {
    id: 7,
    name: 'Anti Virus'
  }
]

export let sitesLists = {
  5: [
    {
      id: 8,
      name: 'Casino Main'
    },
    {
      id: 3,
      name: '888 Main'
    }
  ],

  7: [
    {
      id: 2,
      name: 'Norton Main'
    },
    {
      id: 8,
      name: 'Macafe Main'
    }
  ]
}
