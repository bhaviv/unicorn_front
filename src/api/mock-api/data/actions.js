const actionParams1 = {
  selectedBrands: [ { 'position': 1, 'slug': '888-ladies', 'name': '888 Ladies' }, { 'position': 3, 'slug': 'butlers-bingo', 'name': 'Butlers Bingo' }, { 'position': 4, 'slug': 'dotty-bingo', 'name': 'Dotty Bingo' } ],
  matrix: [
    [33, 33, 34],
    [33, 34, 33],
    [34, 33, 33]
  ]
}

const actionParams2 = {
  selectedBrands: [ { 'position': 3, 'slug': 'butlers-bingo', 'name': 'Butlers Bingo' }, { 'position': 4, 'slug': 'dotty-bingo', 'name': 'Dotty Bingo' } ],
  matrix: [
    [33, 34],
    [39, 34]
  ]
}

export const actions1 = [
  {
    id: 1,
    type: 'WeightRotation',
    params: actionParams1
  },
  {
    id: 1,
    type: 'BudgetLimit',
    params: {}
  }
]

export const actions2 = [
  {
    id: 1,
    type: 'WeightRotation',
    params: actionParams2
  },
  {
    id: 2,
    type: 'BudgetLimit',
    params: {}
  }
]

export const actions3 = [
  {
    id: 1,
    type: 'WeightRotation',
    params: actionParams1
  },
  {
    id: 2,
    type: 'BudgetLimit',
    params: {}
  }
]
