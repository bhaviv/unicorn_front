export const actionTypes = [
  {
    name: 'Weight Rotation',
    type: 'WeightRotation'
  },
  {
    name: 'Budget Limit',
    type: 'BudgetLimit'
  }
]

