export const LATENCY = 1000

export let counters = {
  autoIncrementRuleId: 1000,
  autoIncrementExperimentId: 1000
}

export * from './sites.js'
export * from './experiment.js'
export * from './audience.js'
export * from './ruleTypes.js'
export * from './actionTypes.js'
export * from './brandsLists.js'
