import http from './http.js'

export const List = {
  all (siteId) {
    let getListsUrl = buildListUrl(siteId)
    return http.get(getListsUrl)
  },

  get (siteId, listId) {
    let getListUrl = buildListUrl(siteId, listId)
    return http.get(getListUrl)
  },

  save (siteId, listToSave) {
    if (listToSave.id) {
      let updateListUrl = buildListUrl(siteId, listToSave.id)
      return http.put(updateListUrl, listToSave)
    } else {
      let saveListUrl = buildListUrl(siteId)
      return http.post(saveListUrl, listToSave)
    }
  },

  delete (siteId, listId) {
    let deleteListUrl = buildListUrl(siteId, listId)
    console.log('deleteListUrl', deleteListUrl)
    return http.delete(deleteListUrl)
  },

  getContentTypes (siteId) {
    let contentTypesUrl = 'api/v1/sites/' + siteId + '/content-types.json'
    return http.get(contentTypesUrl)
  },

  getBrands (siteId, contentTypeId) {
    let getBrandsUrl = 'api/v1/sites/' + siteId + '/content-types/' + contentTypeId + '.json'
    return http.get(getBrandsUrl)
  },

  toggleBrand (siteId, brandId, type) {
    let toggleBrandUrl = 'api/v1/sites/' + siteId + '/toggle-brand/' + brandId + '/type/' + type + '.json'
    return http.post(toggleBrandUrl)
  }
}

function buildListUrl (siteId, listId) {
  let url = 'api/v1/sites/' + siteId + '/lists'
  url += (listId ? '/' + listId : '') + '.json'

  return url
}
