import http from './http.js'

export const PositionsScore = {
  get (siteId, contentTypeId) {
    let getPositionsUrl = buildPositionsUrl(siteId, contentTypeId)
    return http.get(getPositionsUrl)
  },

  save (siteId, contentTypeId, positionsToSave) {
    let savePositionsUrl = buildPositionsUrl(siteId, contentTypeId)
    return http.post(savePositionsUrl, positionsToSave)
  }
}

function buildPositionsUrl (siteId, contentTypeId) {
  let url = 'api/v1/sites/' + siteId + '/content-types/' + contentTypeId + '/positions-score.json'
  return url
}
