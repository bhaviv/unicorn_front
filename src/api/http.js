import axios from 'axios'

/**
 * Wrapper for http requests
 */
let Http = {
  httpErrorHandler (error) {
    console.log('Temp event handler should be override', error)
    throw error
  },

  get (relativeUrl, data) {
    return this.send('get', relativeUrl, data)
  },

  post (relativeUrl, data) {
    return this.send('post', relativeUrl, data)
  },

  put (relativeUrl, data) {
    return this.send('put', relativeUrl, data)
  },

  delete (relativeUrl) {
    return this.send('delete', relativeUrl)
  },

  send (method, relativeUrl, data) {
    let url = window.Uni.env.BACKEND_URL + '/' + relativeUrl
    let jwtToken = window.Uni.token
    let headers = {'jwt_token': jwtToken}

    return new Promise((resolve, reject) => {
      axios({
        headers: headers,
        method: method,
        url: url,
        data: data,
        withCredentials: true
      })
      .then(resolve)
      .catch(error => {
        try {
          Http.httpErrorHandler(error)
        } catch (e) {
          reject(e)
        }
      })
    })
  }
}

export default Http
