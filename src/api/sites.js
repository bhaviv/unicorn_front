import http from './http.js'

export const Site = {
  all () {
    return http.get('api/v1/external/sites.json')
  },

  get (siteId) {
    return http.get('api/v1/external/sites/' + siteId + '.json')
  },

  getRibbons (siteId) {
    return http.get('api/v1/sites/' + siteId + '/ribbons.json')
  }
}
