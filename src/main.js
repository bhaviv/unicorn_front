// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App'
import router from './router'
import store from './store'
import VeeValidate from 'vee-validate'
import VueCookie from 'vue-cookie'

Vue.use(VueCookie)
Vue.use(Vuetify)
Vue.use(VeeValidate)
Vue.config.productionTip = false

// Vue for initiate token
let Uni = window.Uni = {}
Uni.env = process.env
Uni.vue = new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {App},
  created () {
    this.getToken()
  },
  methods: {
    getToken () {
      let jwtQueryToken = this.$route.query.token
      let jwtToken = jwtQueryToken || this.$cookie.get('jwt-token')

      if (jwtToken) {
        this.$cookie.set('jwt-token', jwtToken, {expires: '1h'})
        window.Uni.token = jwtToken
        return
      }

      // console.log('there is no token for you: redirect')
      window.location.replace(process.env.LOGIN_URL)
    }
  }
})

