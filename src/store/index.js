import Vue from 'vue'
import Vuex from 'vuex'
import experiment from './modules/experiment'
import ruleTypes from './modules/ruleTypes'
import actionTypes from './modules/actionTypes'
import ribbons from './modules/ribbons'
import brandList from './modules/brandList'
import site from './modules/sites'
import listPlacement from './modules/listPlacements'
import list from './modules/lists'
import positionsScore from './modules/positionsScore'
import alert from './modules/alert'
import http from '../api/http.js'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    alert,
    site,
    list,
    listPlacement,
    ruleTypes,
    actionTypes,
    ribbons,
    positionsScore,
    brandList,
    experiment
  }
})

http.httpErrorHandler = (error) => {
  switch (error.response.status) {
    case 401:
      window.location.replace(window.Uni.env.LOGIN_URL)
      break

    case 403:
      store.dispatch('showAlert', {
        color: 'error',
        text: 'You don\'t have permission for this operation'
      })
      break

    case 404:
      store.dispatch('showAlert', {
        color: 'error',
        text: 'This operation is not exists 404'
      })
      break

    case 500:
      store.dispatch('showAlert', {
        color: 'error',
        text: 'Unknown server Error 500'
      })
      break

    default:
      throw error
  }
}

export default store
