import * as types from '../mutationTypes.js'

const state = {
  alert: {}
}

const mutations = {
  [types.SET_ALERT] (state, alert) {
    state.alert = alert
  }
}

const actions = {
  showAlert: ({commit}, alert) => {
    commit(types.SET_ALERT, alert)
  }
}

const getters = {
  alert () {
    return state.alert
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
