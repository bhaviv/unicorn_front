import * as Api from '../../api'
import * as types from '../mutationTypes.js'

const state = {
  currentList: {
    name: '',
    description: '',
    type: ''
  },
  lists: [],
  contentTypes: []
}

const mutations = {
  [types.SET_CURRENT_LIST] (state, currentList) {
    state.currentList = currentList
  },

  [types.SET_LISTS] (state, {lists}) {
    state.lists = lists
  },

  [types.SET_CONTENT_TYPES] (state, contentTypes) {
    state.contentTypes = contentTypes
  }
}

const actions = {
  setCurrentList: ({commit}, list) => {
    commit(types.SET_CURRENT_LIST, list)
  },

  getLists: ({commit}, siteId) => {
    return Api.List
      .all(siteId)
      .then(result => {
        let lists = result.data.data
        commit(types.SET_LISTS, {lists})
      })
  },

  getList: ({commit}, params) => {
    return Api.List
      .get(params.siteId, params.listId)
      .then(result => {
        let list = result.data.data
        commit(types.SET_CURRENT_LIST, list)
      })
  },

  saveList: ({commit, rootState}, list) => {
    return new Promise((resolve, reject) => {
      console.log('store/saveList', list)
      Api.List
      .save(rootState.site.currentSite.id, list)
      .then(result => {
        console.log('after saved list', result)
        let savedList = result.data.data
        commit(types.SET_CURRENT_LIST, savedList)
        resolve(savedList)
      })
    })
  },

  deleteList: ({commit, rootState, state}) => {
    console.log('deleteList', rootState.site.currentSite.id, state.currentList.id)
    let siteId = rootState.site.currentSite.id
    let listId = state.currentList.id
    return new Promise((resolve, reject) => {
      Api.List
        .delete(siteId, listId)
        .then(result => {
          resolve(result.data.data)
        })
    })
  },

  getContentTypes: ({commit}, siteId) => {
    console.log('site and list', siteId)
    commit(types.SET_CONTENT_TYPES, [])
    return Api.List
      .getContentTypes(siteId)
      .then(result => {
        let contentTypes = result.data.data
        commit(types.SET_CONTENT_TYPES, contentTypes)
      })
  }
}

const getters = {
  currentList () {
    return state.currentList
  },

  contentTypes () {
    return state.contentTypes
  },

  lists () {
    return state.lists
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
