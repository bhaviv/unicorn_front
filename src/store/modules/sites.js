import * as Api from '../../api'
import * as types from '../mutationTypes.js'

const state = {
  currentSite: {},
  sites: []
}

const mutations = {
  [types.SET_CURRENT_SITE] (state, currentSite) {
    state.currentSite = currentSite
  },

  [types.SET_SITES_LIST] (state, {sites}) {
    state.sites = sites
  }
}

const actions = {
  setCurrentSite: ({commit}, site) => {
    console.log('setCurrentSite', site)
    commit(types.SET_CURRENT_SITE, site)
  },

  getSite: ({commit, rootState}) => {
    commit(types.SET_CURRENT_SITE, { id: rootState.listParams.siteId })
    return Api.Site
      .get(rootState.listParams.siteId)
      .then(result => {
        let site = result.data.data
        commit(types.SET_CURRENT_SITE, site)
      })
  },

  getSites: ({commit}) => {
    return Api.Site
      .all()
      .then(result => {
        let sites = result.data.data
        commit(types.SET_SITES_LIST, {sites})
      })
  }
}

const getters = {
  currentSite () {
    return state.currentSite
  },

  sites () {
    return state.sites
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}

