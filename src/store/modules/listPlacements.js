import * as Api from '../../api'
import * as types from '../mutationTypes.js'

const state = {
  listPlacement: {},
  listsPlacements: []
}

const mutations = {
  [types.SET_LISTS_PLACEMENTS] (state, listsPlacements) {
    state.listsPlacements = listsPlacements
  },
  [types.SET_LIST_PLACEMENT] (state, listPlacement) {
    state.listPlacement = listPlacement
  }
}

const actions = {
  getListPlacement: ({commit, rootState}, listPlacementId) => {
    commit(types.SET_LIST_PLACEMENT, {id: listPlacementId})
    return Api.ListPlacement
      .get(rootState.site.currentSite.id, listPlacementId)
      .then(result => {
        let listPlacement = result.data.data
        console.log('store/getListPlacement', listPlacement)
        commit(types.SET_LIST_PLACEMENT, listPlacement)
      })
  },

  getListPlacements: ({commit}, siteId) => {
    return Api.ListPlacement
      .all(siteId)
      .then(result => {
        let listsPlacements = result.data.data
        console.log('listsPlacements', listsPlacements)
        commit(types.SET_LISTS_PLACEMENTS, listsPlacements)
      })
  },

  setListPlacement: ({commit}, lp) => {
    commit(types.SET_LIST_PLACEMENT, lp)
  },

  saveListPlacement: ({commit, rootState}) => {
    return new Promise((resolve, reject) => {
      Api.ListPlacement
      .save(rootState.site.currentSite.id, state.listPlacement)
      .then(result => {
        let savedList = result.data.data
        commit(types.SET_LIST_PLACEMENT, savedList)
        resolve(savedList)
      })
    })
  }
}

// START HERE FETCH THE LIST PLACEMENT
const getters = {
  listPlacement () {
    return state.listPlacement
  },

  listsPlacements () {
    return state.listsPlacements
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}

