import * as Api from '../../api'
import * as types from '../mutationTypes.js'

const state = {
  ribbons: []
}

const mutations = {
  [types.SET_RIBBONS] (state, {ribbons}) {
    state.ribbons = ribbons
  }
}

const actions = {
  getRibbons: ({commit}, siteId) => {
    Api.Site
      .getRibbons(siteId)
      .then(ribbons => {
        ribbons = ribbons.data.data
        commit(types.SET_RIBBONS, {ribbons})
      })
  }
}

const getters = {
  ribbons () {
    return state.ribbons
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
