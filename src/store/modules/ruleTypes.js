import * as Api from '../../api'
import * as types from '../mutationTypes.js'

const state = {
  ruleTypes: []
}

const mutations = {
  [types.SET_RULE_TYPES] (state, {ruleTypes}) {
    state.ruleTypes = ruleTypes
  }
}

const actions = {
  getRuleTypes: ({commit}) => {
    Api.RuleTypes
      .get()
      .then(ruleTypes => {
        ruleTypes = ruleTypes.data.data
        commit(types.SET_RULE_TYPES, {ruleTypes})
      })
  }
}

const getters = {
  ruleTypes () {
    return state.ruleTypes
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}

