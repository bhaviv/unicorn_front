import * as Api from '../../api'
import * as types from '../mutationTypes.js'

const state = {
  actionTypes: []
}

const mutations = {
  [types.SET_ACTION_TYPES] (state, {actionTypes}) {
    state.actionTypes = actionTypes
  }
}

const actions = {
  getActionTypes: ({commit}) => {
    Api.ActionTypes
      .get()
      .then(actionTypes => {
        actionTypes = actionTypes.data.data
        commit(types.SET_ACTION_TYPES, {actionTypes})
      })
  }
}

const getters = {
  actionTypes () {
    return state.actionTypes
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}

