import * as Api from '../../api'
import * as types from '../mutationTypes.js'

const state = {
  currentExperiment: {},
  experiments: []
}

// mutations are operations that actually mutates the state.
// each mutation handler gets the entire state tree as the
// first argument, followed by additional payload arguments.
// mutations must be synchronous and can be recorded by plugins
// for debugging purposes.
const mutations = {
  [types.SET_EXPERIMENT] (state, experiment) {
    state.currentExperiment = experiment
  },

  [types.SET_EXPERIMENTS] (state, {experiments}) {
    state.experiments = experiments
  }
}

// actions are functions that cause side effects and can involve
// asynchronous operations.
const actions = {
  getExperiment: ({dispatch, commit, rootState}, experimentId) => {
    console.log('store/getExperiment', experimentId)
    let lpParams = getLpParams(rootState)
    // commit(types.SET_EXPERIMENT, {})
    return new Promise((resolve, reject) => {
      Api.Experiment.get(lpParams, experimentId)
        .then(result => {
          let experiment = result.data.data
          commit(types.SET_EXPERIMENT, experiment)
          resolve(experiment)
        })
    })
  },

  getExperiments: ({commit, rootState}) => {
    let lpParams = getLpParams(rootState)
    Api.Experiment.all(lpParams)
      .then(result => {
        let experiments = result.data.data
        commit(types.SET_EXPERIMENTS, {experiments})
      })
  },

  createExperiment: ({commit, rootState, state}) => {
    let experiment = JSON.parse(JSON.stringify(state.currentExperiment))
    let lpParams = getLpParams(rootState)
    return new Promise((resolve, reject) => {
      Api.Experiment
        .create(lpParams, experiment)
        .then(result => {
          let experiment = result.data.data
          commit(types.SET_EXPERIMENT, experiment)
          resolve(experiment)
        })
    })
  },

  updateExperiment: ({commit, rootState, state}) => {
    let experiment = JSON.parse(JSON.stringify(state.currentExperiment))
    let lpParams = getLpParams(rootState)
    return new Promise((resolve, reject) => {
      Api.Experiment
        .update(lpParams, experiment)
        .then(result => {
          let experiment = result.data.data
          commit(types.SET_EXPERIMENT, experiment)
          resolve(experiment)
        })
    })
  },

  cloneExperiment: ({commit, rootState, state, dispatch}) => {
    let experiment = JSON.parse(JSON.stringify(state.currentExperiment))
    let lpParams = getLpParams(rootState)
    return new Promise((resolve, reject) => {
      Api.Experiment
        .clone(lpParams, experiment)
        .then(result => {
          let receviedExperiment = result.data.data
          commit(types.SET_EXPERIMENT, receviedExperiment)
          resolve(receviedExperiment)
        })
    })
  },

  deleteExperiment: ({commit, rootState, state}) => {
    let lpParams = getLpParams(rootState)
    return Api.Experiment
      .delete(lpParams, state.currentExperiment)
  },

  stopExperiment: ({commit, rootState, state}) => {
    let experiment = JSON.parse(JSON.stringify(state.currentExperiment))
    let lpParams = getLpParams(rootState)
    return new Promise((resolve, reject) => {
      Api.Experiment
        .stop(lpParams, experiment)
        .then(result => {
          let receviedExperiment = result.data.data
          commit(types.SET_EXPERIMENT, receviedExperiment)
          resolve(receviedExperiment)
        })
    })
  },

  activateExperiment: ({commit, rootState, state}) => {
    let experiment = JSON.parse(JSON.stringify(state.currentExperiment))
    let lpParams = getLpParams(rootState)
    return new Promise((resolve, reject) => {
      Api.Experiment
        .activate(lpParams, experiment)
        .then(result => {
          let receviedExperiment = result.data.data
          commit(types.SET_EXPERIMENT, receviedExperiment)
          resolve(receviedExperiment)
        })
    })
  },

  setEmptyExperiment: ({dispatch, commit, rootState}) => {
    let experiment = createNewExperiment(rootState)
    commit(types.SET_EXPERIMENT, experiment)
  }
}

const getters = {
  currentExperiment () {
    return state.currentExperiment
  },

  experiments () {
    return state.experiments
  }
}

function createNewExperiment (rootState) {
  let lpParams = getLpParams(rootState)
  let experiment = {
    name: '',
    description: '',
    status: 'new',
    siteId: lpParams.siteId,
    listPlacementId: lpParams.lpId,
    rules: {},
    actions: []
  }

  return experiment
}

function getLpParams (rootState) {
  return {
    siteId: rootState.site.currentSite.id,
    lpId: rootState.listPlacement.listPlacement.id
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}

