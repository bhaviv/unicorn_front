import * as Api from '../../api'
import * as types from '../mutationTypes.js'

const state = {
  brandsList: [],
  brandForSettings: {}
}

const mutations = {
  [types.SET_BRANDS_LIST] (state, brandsList) {
    state.brandsList = brandsList
  }
}

const actions = {
  getBrandsList: ({commit, rootState}, params) => {
    Api.List
      .getBrands(params.siteId, params.contentTypeId)
      .then(result => {
        let brands = result.data.data
        commit(types.SET_BRANDS_LIST, brands)
      })
  },
  toggleBrand ({commit}, params) {
    return Api.List
      .toggleBrand(params.siteId, params.brandId, params.type)
  },
  setBrandForSetting ({commit}, brand) {
    state.brandForSettings = brand
  }
}

const getters = {
  brandsList () {
    return state.brandsList
  },
  brandForSettings () {
    return state.brandForSettings
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}

