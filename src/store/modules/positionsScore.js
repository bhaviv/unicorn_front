import * as Api from '../../api'
import * as types from '../mutationTypes.js'

const state = {
  positionsScore: []
}

const mutations = {
  [types.SET_POSITIONS_SCORE] (state, positionsScore) {
    state.positionsScore = positionsScore
  }
}

const actions = {
  setPostions: ({commit}, positionsScore) => {
    commit(types.SET_POSITIONS_SCORE, positionsScore)
  },

  getPositionsScore: ({commit, rootState}, contentTypeId) => {
    return Api.PositionsScore
      .get(rootState.site.currentSite.id, contentTypeId)
      .then(result => {
        let positionScore = result.data.data
        commit(types.SET_POSITIONS_SCORE, positionScore)
      })
  },

  savePositionsScore: ({commit, state, rootState}, contentTypeId) => {
    return new Promise((resolve, reject) => {
      Api.PositionsScore
        .save(rootState.site.currentSite.id, contentTypeId, state.positionsScore)
        .then(result => {
          let savedPositions = result.data.data
          commit(types.SET_POSITIONS_SCORE, savedPositions)
          resolve(savedPositions)
        })
    })
  }
}

const getters = {
  positionsScore () {
    return state.positionsScore
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
