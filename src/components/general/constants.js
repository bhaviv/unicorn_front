export const STATUS = {
  new: 'new',
  draft: 'draft',
  archive: 'archive',
  active: 'active'
}

export const LIST_CATEGORIES = [
  'General',
  'SEO',
  'PPC'
]
