import Vue from 'vue'
import Router from 'vue-router'
import SiteView from '@/components/nav/SiteView'
import ExperimentList from '@/components/experiment/List'
import ExperimentView from '@/components/experiment/View'
import Logout from '@/components/Logout'
import ListsView from '@/components/lists/View'
import ListsPlacements from '@/components/listsPlacements/List'
import ListPlacementEdit from '@/components/listsPlacements/Edit'
import PositionsScoreEdit from '@/components/positionsScore/Edit'
import PositionsScoreList from '@/components/positionsScore/ContentTypeList'

Vue.use(Router)
let router = new Router({
  routes: [
    {
      path: '/sites/:siteId/list',
      name: 'ListsView',
      props: true,
      component: ListsView
    },
    {
      path: '/sites/:siteId/list/create',
      name: 'ListCreate',
      props: true,
      component: ListsView
    },
    {
      path: '/sites/:siteId/list/:listId/edit',
      name: 'ListEdit',
      props: true,
      component: ListsView
    },
    {
      path: '/sites/:siteId/positions-score',
      name: 'PositionsScore',
      props: true,
      component: PositionsScoreList
    },
    {
      path: '/sites/:siteId/positions-score/content-type/:contentTypeId',
      name: 'PositionsScoreEdit',
      props: true,
      component: PositionsScoreEdit
    },
    {
      path: '/sites/:siteId',
      name: 'SiteView',
      props: true,
      component: SiteView
    },
    {
      path: '/sites/:siteId/list-placement',
      name: 'ListsPlacements',
      props: true,
      component: ListsPlacements
    },
    {
      path: '/sites/:siteId/list-placement/create',
      name: 'ListsPlacementCreate',
      props: true,
      component: ListPlacementEdit
    },
    {
      path: '/sites/:siteId/list-placement/:lpId/edit',
      name: 'ListPlacementEdit',
      props: true,
      component: ListPlacementEdit
    },
    {
      path: '/sites/:siteId/list-placements/:lpId/experiments/create',
      name: 'ExperimentCreate',
      component: ExperimentView
    },
    {
      path: '/sites/:siteId/list-placements/:lpId/experiments/:experimentId',
      name: 'ExperimentView',
      component: ExperimentView
    },
    {
      path: '/sites/:siteId/list-placements/:lpId/experiments',
      name: 'ExperimentList',
      props: true,
      component: ExperimentList
    },
    {
      path: '/logout',
      name: 'Logout',
      component: Logout
    }
  ]
})

router.afterEach((to, from) => {
  window.onbeforeunload = null
})

export default router

