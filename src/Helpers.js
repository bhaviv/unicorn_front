export default {
  isEmpty: (obj) => {
    return Object.keys(obj).length === 0
  },

  arrayToObject: (array, keyField) =>
   array.reduce((obj, item) => {
     obj[item[keyField]] = item
     return obj
   }, {}),

  objectDeepClone: (objectToClone) => {
    return JSON.parse(JSON.stringify(objectToClone))
  }
}
